import { useState } from 'react'
import React from 'react';

const CurrencyForm = () =>  {

    const [currencies, setCurrencies] = useState([
        { name: 'USD', exchangeRate: 1, default: true },
        { name: 'GBP', exchangeRate: "", default: false },
        { name: 'EUR', exchangeRate: "", default: false },
    ]);

    const [defaultCurrency, setDefaultCurrency] = useState(currencies[0].name);
    const [file, setFile] = useState(undefined);
    const [vat, setVat] = useState('');
    const [invoiceData, setInvoiceData] = useState([]);
    const [errors, setErrors] = useState('');

    const handleDefaultCurrencyChange = e => {
        setDefaultCurrency(e.target.value)
        setCurrencies(
            currencies.map(c => c.name === e.target.value ? { ...c, exchangeRate: 1, default: true } : { ...c, default: false })
        )
    };

    const handleFileUpload = e => setFile(e.target.files[0]);
    const handleVat = e => setVat(e.target.value);

    const handleExchangeRateChange = e => {
        setCurrencies(
           currencies.map(c => c.name === e.target.name ? { ...c, exchangeRate: +e.target.value } : c)
        )
    }

    const handleValidation = () => {
        let isValid = true;

        if (!file) {
            isValid = false;
            setErrors('The file is empty.');
        }

        if (currencies.filter(c => c.exchangeRate === '').length > 0) {
            isValid = false;
            setErrors('Input fields cannot be empty.');
        }


        return isValid;
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();

        const valid = handleValidation();
        if (!valid) return;
        setErrors('');

        formData.append('file', file);
        formData.append('currencies', JSON.stringify(currencies));
        formData.append('vat', JSON.stringify(vat));

        fetch(`${window.location.href}currencies-data`, {
            method: 'POST',
            body: formData
        })
        .then(res => res.json())
        .then(data => setInvoiceData(data.data));
    }

    return (
        <>
            <form method="post">
                <div>
                    Set Default Currency
                    <select value={defaultCurrency} onChange={handleDefaultCurrencyChange}>
                        {currencies.map((currency, i) => <option key={i} value={currency.name}>{currency.name}</option>)}
                    </select>
                </div>
                Set Exchange Rate
                <div>{currencies.map((currency, i) =>
                    <div key={i + 1}>
                        <label>
                            {currency.name}
                            <input type="text"
                                value={currency.exchangeRate}
                                name={currency.name}
                                onChange={handleExchangeRateChange}
                            />
                        </label>
                    </div>)}
                </div>
                Upload File
                <div>
                    <input type="file" onChange={handleFileUpload} />
                </div>
                Filter by VAT (optional)
                <div>
                    <input type="text" onChange={handleVat} />
                </div>
                <input type="submit" value="Submit" onClick={handleSubmit} />
            </form>
            {
                errors != ''  &&
                <p> {errors} </p>
            }
            {
                invoiceData.length > 0 &&
                invoiceData.map((i, idx) => { return <div key={idx}> {i.customer} | {i.total} | {i.currency} </div> })
            }
        </>
    );
}

export default CurrencyForm;