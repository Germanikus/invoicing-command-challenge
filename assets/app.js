// import './styles/app.css';
// import './bootstrap';
import React, { Component } from 'react';
import  ReactDOM  from 'react-dom';
import CurrencyForm  from './components/CurrencyForm';

const App = () => {
  return (
    <div>
      <CurrencyForm></CurrencyForm>
    </div>
  );
}

ReactDOM.render(<App/>, document.getElementById('root'));