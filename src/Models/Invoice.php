<?php
namespace App\Models;

use App\Models\Currency;

class Invoice {

    public $customer;
    public $vatNumber;
    public $documentNumber;
    public $type;
    public $parentDocument;
    public $currency;
    public $total;

    const INVOICE = 1;
    const CREDIT_NOTE = 2;
    const DEBIT_NOTE = 3;

    public function __construct(
        $customer,
        $vatNumber,
        $documentNumber,
        $type,
        $parentDocument,
        $currency,
        $total)
    {
        $this->customer = $customer;
        $this->vatNumber = $vatNumber;
        $this->documentNumber = $documentNumber;
        $this->type = $type;
        $this->parentDocument = $parentDocument;
        $this->currency = $currency;
        $this->total = $total;
    }


}