<?php
namespace App\Models;

class Currency {

    public $name;
    public $exchangeRate;
    public $default;

    public function __construct($name, $exchangeRate, $default) {
        $this->name = $name;
        $this->exchangeRate = $exchangeRate;
        $this->default = $default;
    }
}