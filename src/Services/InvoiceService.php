<?php
namespace App\Services;

use App\Models\Invoice;
use App\Models\Currency;

class InvoiceService
{
    public $currencies = [];
    public $data = [];

    public function setData($data): void {
        array_shift($data);
        $invoices = [];

        foreach ($data as $value) {
            $vatNumber = $value[1];
            $invoice = new Invoice(
                $value[0],
                intval($vatNumber),
                intval($value[2]),
                intval($value[3]),
                intval($value[4]),
                $value[5],
                intval($value[6]),
            );
            $invoice = $this->setInvoiceCurrency($invoice);
            $invoices[] = $invoice;
        }

        foreach ($invoices as $invoice) {
            if (!array_key_exists($invoice->vatNumber, $this->data)) {
                $this->data[$invoice->vatNumber] = $invoice;
            }
            else {
                $documentNumber = $this->data[$invoice->vatNumber]->documentNumber;
                $invoiceType = $this->data[$invoice->vatNumber]->type;

                if ($documentNumber === $invoice->parentDocument) {
                    if ($invoiceType === Invoice::CREDIT_NOTE) {
                        $this->data[$invoice->vatNumber]->total -= $invoice->total;
                    }
                }
                else {
                    $this->data[$invoice->vatNumber]->total += $invoice->total;
                }
            }
        }
    }

    private function setInvoiceCurrency($invoice): Invoice {
        $defaultCurrency = $this->getDefaultCurrency($this->currencies);

        foreach ($this->currencies as $currency) {
            if ($invoice->currency == $currency->name) {
                if (!isset($currency->exchangeRate)) continue;
                $invoice->total = $invoice->total * $currency->exchangeRate;
                $invoice->currency = $defaultCurrency;
            }
        }
        return $invoice;
    }

    public function setCurrencies($currencies): void {
        foreach ($currencies as $key => $value) {
            $currency = new Currency($value['name'], $value['exchangeRate'], $value['default']);
            $this->currencies[] = $currency;
        }
    }

    private function getDefaultCurrency($currencies): string
    {
        foreach ($currencies as $currency) {
            if ($currency->default) {
                return $currency->name;
            }
        }
    }

}