<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Services\FileUploader;
use App\Models\Invoice;
use App\Services\InvoiceService;

class DocumentsController extends AbstractController
{
    /**
     * @Route("/documents", name="documents")
     */
    public function index(): Response
    {
        return $this->render('documents/index.html.twig', [
            'controller_name' => 'DocumentsController',
        ]);
    }

    public function postCurrenciesData(Request $request, FileUploader $fileUploader, InvoiceService $invoiceService)
    {
        $fileName = $fileUploader->upload($request->files->get("file"));
        $filePath = $fileUploader->getTargetDirectory() . '/' . $fileName;

        $csv = array_map('str_getcsv', file($filePath));
        $currencies = json_decode($request->get('currencies'), true);

        $invoiceService->setCurrencies($currencies);
        $invoiceService->setData($csv);

        return new JsonResponse(array('data' => array_values($invoiceService->data)));
    }

}
